package com.jjcorsif.todoapp.ui.api.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.util.List;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;

@WebMvcTest(TodoItemApiController.class)
public class TodoItemApiControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TodoItemApiController todoItemApiController;
	
	@Test
	public void allTodoItemsAreReturnedCorrectedly() throws Exception {
		this.mockMvc.perform(get("/api/todos").contentType("application/json;charset=utf-8")).andExpect(status().isOk());
	}
	
	@Test
	public void aTodoItemIsAddedCorrectedly() throws Exception {
		UUID todoListId = UUID.randomUUID();
		this.mockMvc.perform(
			post("/api/todos/" + todoListId)
			.contentType("application/json;charset=utf-8")
			.content(asJsonString(new TodoItem(UUID.randomUUID(), todoListId, "testar a aplicação")))
		)
		.andExpect(status().isCreated());
	}
	
	private String asJsonString(TodoItem todoItem) {
		return "{ \"id\": \"" + todoItem.getId() + "\", \"todoListId\": \"" + todoItem.getTodoListId() + "\", \"description\": \"" + todoItem.getDescription() + "\"}";
	}
	
	@Test
	public void aTodoItemIsUpdatedCorrectedly() throws Exception {
		List<TodoItem> listOfTodoItems = this.todoItemApiController.findAll();
		if (listOfTodoItems.isEmpty()) {
			// this.todoItemApiController.insert(UUID.randomUUID(), new TodoItem(UUID.randomUUID(), UUID.randomUUID(), "testar a aplicação"));
			return;
		}
		TodoItem todoItem = listOfTodoItems.get((int) (Math.random() * listOfTodoItems.size()));
		UUID todoItemId = todoItem.getId();
		this.mockMvc.perform(
			delete("/api/todos/" + todoItemId)
			.contentType("application/json;charset=utf-8")
			.content(asJsonString(new TodoItem(todoItem.getId(), todoItem.getTodoListId(), "Retestar a aplicação")))
		)
		.andExpect(status().isNoContent());
	}
	
	@Test
	public void aTodoItemIsDeletedCorrectedly() throws Exception {
		List<TodoItem> listOfTodoItems = this.todoItemApiController.findAll();
		if (listOfTodoItems.isEmpty()) {
			// this.todoItemApiController.insert(UUID.randomUUID(), new TodoItem(UUID.randomUUID(), UUID.randomUUID(), "testar a aplicação"));
			return;
		}
		TodoItem todoItem = listOfTodoItems.get((int) (Math.random() * listOfTodoItems.size()));
		UUID todoItemId = todoItem.getId();
		this.mockMvc.perform(
			put("/api/todos/" + todoItemId)
			.contentType("application/json;charset=utf-8")
		).andExpect(status().isNoContent());
	}
}
