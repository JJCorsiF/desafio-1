package com.jjcorsif.todoapp.infrastructure.persistence;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.application.repositories.TodoListRepository;
import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("postgresTodoList")
public class TodoListPostgresRepository implements TodoListRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public TodoListPostgresRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public List<TodoList> findAll() {
		final String sql = "SELECT id_todo_list, name FROM todo_list;";
		
		return this.jdbcTemplate.query(sql, (resultSet, i) -> {
				UUID id = UUID.fromString(resultSet.getString("id_todo_list"));
				String name = resultSet.getString("name");
				
				return new TodoList(id, name);
			});
	}

	@Override
	public Optional<TodoList> findById(UUID id) {
		final String sql = "SELECT id_todo_list, name FROM todo_list WHERE id_todo_list = ?;";
		Object[] args = new Object[]{id};
		List<TodoList> listOfTodoLists = this.jdbcTemplate.query(sql, args, (resultSet, i) -> {
			UUID todoListId = UUID.fromString(resultSet.getString("id_todo_list"));
			String name = resultSet.getString("name");
			
			return new TodoList(todoListId, name);
		});
		
		if (listOfTodoLists.isEmpty()) {
			return Optional.ofNullable(null);
		}
		
		TodoList todoList = listOfTodoLists.get(0);
		
		return Optional.ofNullable(todoList);
	}

	@Override
	public TodoList insert(UUID id, TodoList todoList) {
		final String sql = "INSERT INTO todo_list (id_todo_list, name) VALUES (?, ?) ON CONFLICT (id_todo_list) DO NOTHING;";
		
		String name = todoList.getName();
		
		this.jdbcTemplate.update(sql, id, name);
		
		return new TodoList(id, name);
	}

	@Override
	public int deleteTodoList(UUID id) {
		final String sql = "DELETE FROM todo_list WHERE id_todo_list = ?;";
		
		return this.jdbcTemplate.update(sql, id);
	}

	@Override
	public int deleteAllTodoLists() {
		final String sql = "DELETE FROM todo_list;";
		
		return this.jdbcTemplate.update(sql);
	}
}
