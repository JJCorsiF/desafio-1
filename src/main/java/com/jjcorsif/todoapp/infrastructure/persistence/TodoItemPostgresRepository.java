package com.jjcorsif.todoapp.infrastructure.persistence;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.application.repositories.TodoItemRepository;
import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("postgres")
public class TodoItemPostgresRepository implements TodoItemRepository {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public TodoItemPostgresRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<TodoItem> findAll() {
		final String sql = "SELECT id, id_todo_list, description, done FROM todo_item;";
		return this.jdbcTemplate.query(sql, (resultSet, i) -> {
				UUID id = UUID.fromString(resultSet.getString("id"));
				String todoListIdString = resultSet.getString("id_todo_list");
				UUID todoListId = todoListIdString == null ? null : UUID.fromString(todoListIdString);
				String description = resultSet.getString("description");
				boolean done = resultSet.getBoolean("done");
				
				TodoItem todoItem = new TodoItem(id, todoListId, description);
				
				if (done) {
					todoItem.complete();
				}
				
				return todoItem;
			});
	}
	
	@Override
	public List<TodoItem> findAllByTodoListId(UUID todoListId) {
		final String sql = "SELECT id, description, done FROM todo_item WHERE id_todo_list = ?;";
		Object[] args = new Object[]{todoListId};
		return this.jdbcTemplate.query(sql, args, (resultSet, i) -> {
				UUID id = UUID.fromString(resultSet.getString("id"));
				String description = resultSet.getString("description");
				boolean done = resultSet.getBoolean("done");
				
				TodoItem todoItem = new TodoItem(id, todoListId, description);
				
				if (done) {
					todoItem.complete();
				}
				
				return todoItem;
		});
	}

	@Override
	public Optional<TodoItem> findById(UUID id) {
		final String sql = "SELECT id, id_todo_list, description, done FROM todo_item WHERE id = ?;";
		Object[] args = new Object[]{id};
		List<TodoItem> listOfTodoItems = this.jdbcTemplate.query(sql, args, (resultSet, i) -> {
			UUID todoItemId = UUID.fromString(resultSet.getString("id"));
			String todoListIdString = resultSet.getString("id_todo_list");
			UUID todoListId = todoListIdString == null ? null : UUID.fromString(todoListIdString);
			String description = resultSet.getString("description");
			boolean done = resultSet.getBoolean("done");
			
			TodoItem newTodoItem = new TodoItem(todoItemId, todoListId, description);
			
			if (done) {
				newTodoItem.complete();
			}
			
			return newTodoItem;
		});
		
		if (listOfTodoItems.isEmpty()) {
			return Optional.ofNullable(null);
		}
		
		TodoItem todoItem = listOfTodoItems.get(0);
		
		return Optional.ofNullable(todoItem);
	}
	
	@Override
	public TodoItem insert(UUID todoListId, UUID id, TodoItem todoItem) {
		final String sql = "INSERT INTO todo_item (id, id_todo_list, description, done) VALUES (?, ?, ?, ?) ON CONFLICT (id) DO NOTHING;";
		
		String description = todoItem.getDescription();
		boolean done = todoItem.isCompleted();
		
		this.jdbcTemplate.update(sql, id, todoListId, description, done);
		
		return new TodoItem(id, todoListId, description);
	}

	@Override
	public int updateTodoItem(UUID id, TodoItem todoItem) {
		final String sql = "UPDATE todo_item SET description = ?, done = ? WHERE id = ?;";
		
		return this.jdbcTemplate.update(sql, todoItem.getDescription(), todoItem.isCompleted(), id);
	}

	@Override
	public int deleteTodoItem(UUID id) {
		final String sql = "DELETE FROM todo_item WHERE id = ?;";
		
		return this.jdbcTemplate.update(sql, id);
	}

	@Override
	public int deleteAllTodoItemsWithTodoListId(UUID todoListId) {
		final String sql = "DELETE FROM todo_item WHERE id_todo_list = ?;";
		
		return this.jdbcTemplate.update(sql, todoListId);
	}
	
	@Override
	public int deleteAllTodoItems() {
		final String sql = "DELETE FROM todo_item;";
		
		return this.jdbcTemplate.update(sql);
	}
}
