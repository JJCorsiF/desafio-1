package com.jjcorsif.todoapp.ui.api.rest;

import java.util.List;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;
import com.jjcorsif.todoapp.service.TodoListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/todolists")
public class TodoListApiController {
	
	private final TodoListService todoListService;
	
	@Autowired
	public TodoListApiController(TodoListService todoListService) {
		this.todoListService = todoListService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<TodoList> findAll() {
		return this.todoListService.findAll();
	}
	
	@GetMapping(path="{id}")
	@ResponseStatus(HttpStatus.OK)
	public TodoList findById(
		@PathVariable("id")
		UUID id
	) {
		return this.todoListService.findById(id).orElse(null);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public TodoList insert(@Validated @NonNull @RequestBody TodoList todoList) {
		return this.todoListService.insert(todoList);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTodoList(@PathVariable("id") UUID id) {
		this.todoListService.deleteTodoList(id);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllTodoLists() {
		this.todoListService.deleteAllTodoLists();
	}
}
