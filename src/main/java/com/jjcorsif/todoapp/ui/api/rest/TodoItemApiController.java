package com.jjcorsif.todoapp.ui.api.rest;

import java.util.List;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;
import com.jjcorsif.todoapp.service.TodoItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(path="/api/todos")
@RestController
public class TodoItemApiController {
	
	private final TodoItemService todoItemService;
	
	@Autowired
	public TodoItemApiController(TodoItemService todoItemService) {
		this.todoItemService = todoItemService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<TodoItem> findAll() {
		return this.todoItemService.findAll();
	}
	
	@GetMapping(path="{todoListId}")
	@ResponseStatus(HttpStatus.OK)
	public List<TodoItem> findAllByTodoListId(@PathVariable("todoListId") UUID todoListId) {
		return this.todoItemService.findAllByTodoListId(todoListId);
	}
	
	// @GetMapping(path="{id}")
	// @ResponseStatus(HttpStatus.OK)
	public TodoItem findById(
		// @PathVariable("id")
		UUID id
	) {
		return this.todoItemService.findById(id).orElse(null);
	}
	
	@PostMapping(path="{todoListId}")
	@ResponseStatus(HttpStatus.CREATED)
	public TodoItem insert(@PathVariable("todoListId") UUID todoListId, @Validated @NonNull @RequestBody TodoItem todoItem) {
		return this.todoItemService.insert(todoListId, todoItem);
	}
	
	@PutMapping(path="{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateTodoItem(@PathVariable("id") UUID id, @Validated @NonNull @RequestBody TodoItem todoItem) {
		this.todoItemService.updateTodoItem(id, todoItem);
	}
	
	@PutMapping(path="/complete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void completeTodoItem(@PathVariable("id") UUID id) {
		TodoItem todoItem = this.findById(id);
		
		if (todoItem == null) {
			return;
		}
		
		todoItem.complete();
		this.updateTodoItem(id, todoItem);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTodoItem(@PathVariable("id") UUID id) {
		this.todoItemService.deleteTodoItem(id);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllTodoItems() {
		this.todoItemService.deleteAllTodoItems();
	}
	
	@DeleteMapping(path="{todoListId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAllTodoItemsWithTodoListId(@PathVariable("todoListId") UUID todoListId) {
		this.todoItemService.deleteAllTodoItemsWithTodoListId(todoListId);
	}
}
