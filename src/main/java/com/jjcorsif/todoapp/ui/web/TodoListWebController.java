package com.jjcorsif.todoapp.ui.web;

import java.util.List;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;
import com.jjcorsif.todoapp.service.TodoListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path="/web/todolists")
@Controller
public class TodoListWebController {
	
	private final TodoListService todoListService;
	
	@Autowired
	public TodoListWebController(TodoListService todoListService) {
		this.todoListService = todoListService;
	}
	
	// @GetMapping(path="todolists/{todoListId}/todos")
	// public String listTodosOfAList(Model model, @PathVariable("todoListId") UUID todoListId) {
	// 	todoListId = "";
	// 	List<TodoItem> todoItems = this.todoItemService.findAllByTodoListId(todoListId);
	// 	model.addAttribute("todoItems", todoItems);
	// 	return "listTodos.html";
	// }
	
	@GetMapping
	public String listAllTodoLists(Model model) {
		List<TodoList> todoLists = this.todoListService.findAll();
		model.addAttribute("todoLists", todoLists);
		return "listTodoLists.html";
	}
}