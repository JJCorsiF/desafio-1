package com.jjcorsif.todoapp.ui.web;

import java.util.List;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;
import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;
import com.jjcorsif.todoapp.service.TodoItemService;
import com.jjcorsif.todoapp.service.TodoListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path="/web/todos")
@Controller
public class TodoItemWebController {
	
	private final TodoItemService todoItemService;
	private final TodoListService todoListService;
	
	@Autowired
	public TodoItemWebController(TodoItemService todoItemService, TodoListService todoListService) {
		this.todoItemService = todoItemService;
		this.todoListService = todoListService;
	}
	
	@GetMapping
	public String listAllTodos(Model model) {
		List<TodoItem> todoItems = this.todoItemService.findAll();
		
		model.addAttribute("todoListId", null);
		model.addAttribute("todoItems", todoItems);
		return "listTodos.html";
	}
	
	@GetMapping(path="{todoListId}")
	public String listTodosOfAList(Model model, @PathVariable("todoListId") UUID todoListId) {
		List<TodoItem> todoItems = this.todoItemService.findAllByTodoListId(todoListId);
		TodoList todoList = this.todoListService.findById(todoListId).orElse(null);
		
		model.addAttribute("todoList", todoList);
		model.addAttribute("todoItems", todoItems);
		return "listTodos.html";
	}
}