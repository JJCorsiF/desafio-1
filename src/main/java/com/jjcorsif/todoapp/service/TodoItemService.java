package com.jjcorsif.todoapp.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.application.repositories.TodoItemRepository;
import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class TodoItemService {
	
	private final TodoItemRepository todoItemRepository;
	
	@Autowired
	public TodoItemService(@Qualifier("postgres") TodoItemRepository todoItemRepository) {
		this.todoItemRepository = todoItemRepository;
	}
	
	public List<TodoItem> findAll() {
		return this.todoItemRepository.findAll();
	}
	
	public Optional<TodoItem> findById(UUID id) {
		return this.todoItemRepository.findById(id);
	}
	
	public List<TodoItem> findAllByTodoListId(UUID todoListId) {
		return this.todoItemRepository.findAllByTodoListId(todoListId);
	}
	
	public TodoItem insert(UUID todoListId, TodoItem todoItem) {
		UUID id = todoItem.getId();
		
		if (id != null) {
			return this.todoItemRepository.insert(todoListId, id, todoItem);
		}
		
		return this.todoItemRepository.insert(todoListId, todoItem);
	}
	
	public int updateTodoItem(UUID id, TodoItem todoItem) {
		return this.todoItemRepository.updateTodoItem(id, todoItem);
	}
	
	public int deleteTodoItem(UUID id) {
		return this.todoItemRepository.deleteTodoItem(id);
	}
	
	public int deleteAllTodoItems() {
		return this.todoItemRepository.deleteAllTodoItems();
	}
	
	public int deleteAllTodoItemsWithTodoListId(UUID todoListId) {
		return this.todoItemRepository.deleteAllTodoItemsWithTodoListId(todoListId);
	}
}
