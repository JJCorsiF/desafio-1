package com.jjcorsif.todoapp.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.application.repositories.TodoListRepository;
import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class TodoListService {
	
	private final TodoListRepository todoListRepository;
	
	@Autowired
	public TodoListService(@Qualifier("postgresTodoList") TodoListRepository todoListRepository) {
		this.todoListRepository = todoListRepository;
	}
	
	public List<TodoList> findAll() {
		return this.todoListRepository.findAll();
	}
	
	public Optional<TodoList> findById(UUID id) {
		return this.todoListRepository.findById(id);
	}
	
	public TodoList insert(TodoList todoList) {
		UUID id = todoList.getId();
		
		if (id != null) {
			return this.todoListRepository.insert(id, todoList);
		}
		
		return this.todoListRepository.insert(todoList);
	}
	
	public int deleteTodoList(UUID id) {
		return this.todoListRepository.deleteTodoList(id);
	}
	
	public int deleteAllTodoLists() {
		return this.todoListRepository.deleteAllTodoLists();
	}
}
