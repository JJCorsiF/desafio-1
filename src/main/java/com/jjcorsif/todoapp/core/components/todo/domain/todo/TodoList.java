package com.jjcorsif.todoapp.core.components.todo.domain.todo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TodoList {
	
	private final UUID id;
	
	private String name;
	
	private List<TodoItem> todoItems;
	
	// public TodoList(@JsonProperty("id") UUID id, @JsonProperty("name") String name, List<TodoItem> todoItems) {
	// 	this.id = id;
	// 	this.name = name;
	// 	this.todoItems = todoItems;
	// }
	
	public TodoList(@JsonProperty("id") UUID id, @JsonProperty("name") String name) {
		this.id = id;
		this.name = name;
		this.todoItems = new ArrayList<>();
	}
	
	@Id
	public UUID getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<TodoItem> getTodoItems() {
		return this.todoItems;
	}
}
