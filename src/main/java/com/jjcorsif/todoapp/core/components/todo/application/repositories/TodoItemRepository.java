package com.jjcorsif.todoapp.core.components.todo.application.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoItem;

public interface TodoItemRepository {
	public List<TodoItem> findAll();
	public List<TodoItem> findAllByTodoListId(UUID todoListId);
	public Optional<TodoItem> findById(UUID id);
	
	public TodoItem insert(UUID todoListId, UUID id, TodoItem todoItem);
	public default TodoItem insert(UUID todoListId, TodoItem todoItem) {
		UUID id = UUID.randomUUID();
		return insert(todoListId, id, todoItem);
	}
	
	public int updateTodoItem(UUID id, TodoItem todoItem);
	
	public int deleteTodoItem(UUID id);
	public int deleteAllTodoItemsWithTodoListId(UUID todoListId);
	public int deleteAllTodoItems();
}
