package com.jjcorsif.todoapp.core.components.todo.application.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.jjcorsif.todoapp.core.components.todo.domain.todo.TodoList;

public interface TodoListRepository {
	public List<TodoList> findAll();
	public Optional<TodoList> findById(UUID id);
	
	public TodoList insert(UUID id, TodoList todoList);
	public default TodoList insert(TodoList todoList) {
		UUID id = UUID.randomUUID();
		return insert(id, todoList);
	}
	
	public int deleteTodoList(UUID id);
	public int deleteAllTodoLists();
}
