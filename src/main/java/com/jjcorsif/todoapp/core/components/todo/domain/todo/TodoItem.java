package com.jjcorsif.todoapp.core.components.todo.domain.todo;

import java.util.UUID;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TodoItem {
	
	private final UUID id;
	
	private UUID todoListId;
	
	private String description;
	
	private boolean completed;
	
	public TodoItem(@JsonProperty("id") UUID id, @JsonProperty("todoListId") UUID todoListId, @JsonProperty("description") String description, @JsonProperty("completed") boolean completed) {
		this.id = id;
		this.todoListId = todoListId;
		this.description = description;
		this.completed = completed;
	}
	
	public TodoItem(UUID id, UUID todoListId, String description) {
		this.id = id;
		this.todoListId = todoListId;
		this.description = description;
		this.completed = false;
	}
	
	public void complete() {
		this.completed = true;
	}
	
	@Id
	public UUID getId() {
		return this.id;
	}
	
	public UUID getTodoListId() {
		return this.todoListId;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public boolean isCompleted() {
		return this.completed;
	}
}
