// import axios from 'https://unpkg.com/axios@0.20.0/dist/axios.min.js';

const createLi = (todoItem) => {
	const li = document.createElement('li');
	li.className = 'todoitem-' + todoItem.id;
	li.addEventListener('click', () => {
		toggleCompleted(li, todoItem.id);
	});
	
	const todoitemDescriptionContainer = document.createElement('div');
	todoitemDescriptionContainer.className = 'todoitem-description-container';
	
	const faCompletedIconClass = (todoItem.completed ? 'fa-check-circle' : 'fa-circle');
	
	const faCompletedIcon = document.createElement('i');
	faCompletedIcon.className = 'far ' + faCompletedIconClass;
	todoitemDescriptionContainer.appendChild(faCompletedIcon);
	
	todoitemDescriptionContainer.appendChild(document.createTextNode(' '));
	
	const todoDescriptionSpan = document.createElement('span');
	todoDescriptionSpan.className = 'todoitem-description-span';
	todoDescriptionSpan.style.textDecoration = (todoItem.completed ? 'line-through' : 'unset');
	todoDescriptionSpan.innerHTML = todoItem.description;
	
	todoitemDescriptionContainer.appendChild(todoDescriptionSpan);
	li.appendChild(todoitemDescriptionContainer);
	
	// const todoItemIdInput = document.createElement('input');
	// todoItemIdInput.type = 'hidden';
	// todoItemIdInput.className = 'todoitem-id';
	// todoItemIdInput.value = todoItem.id;
	// li.appendChild(todoItemIdInput);
	
	const deleteTodoItemButton = document.createElement('button');
	deleteTodoItemButton.type = 'button';
	deleteTodoItemButton.className = 'delete-todoitem-button';
	
	const faDeleteIcon = document.createElement('i');
	faDeleteIcon.className = 'far fa-times-circle'; 
	deleteTodoItemButton.appendChild(faDeleteIcon);
	
	deleteTodoItemButton.addEventListener('click', () => {
		deleteTodoItem(li, todoItem.id);
	});
	
	li.appendChild(deleteTodoItemButton);
	
	return li;
};

const appendToDOM = (todoItem) => {
	const ul = document.querySelector('ul.task-list');
	ul.appendChild(createLi(todoItem));
	document.querySelector('.no-todoitems-to-show').style.display = 'none';
	document.querySelector('.delete-all-todoitems-form').style.display = 'flex';
};

const deleteTodoItem = async (elem, id) => {
	try {
		const response = await axios.delete(`/api/todos/${id}`);
		
		if (response.status !== 204) {
			console.table({response});
			return;
		}

		console.log('DELETE: the todo with id ' + id + ' was removed');
		elem.remove();

		if (document.querySelectorAll('.task-list li').length === 0) {
			document.querySelector('.no-todoitems-to-show').style.display = 'block';
			document.querySelector('.delete-all-todoitems-form').style.display = 'none';
		}
	} catch (error) {
		console.error(error);
	}
};

const deleteAllTodoItems = async () => {
	try {
		const todoListId = document.querySelector('.todolist-id').value;
		const response = await axios.delete(
			'/api/todos' + (todoListId.length > 0 ? '/' + todoListId : '')
		);

		if (response.status !== 204) {
			console.table({ response });
			return;
		}
		
		console.log('DELETE: all todos were removed');
		document.querySelectorAll('.task-list li').forEach((li) => {
			li.remove();
		});

		document.querySelector('.no-todoitems-to-show').style.display = 'block';
		document.querySelector('.delete-all-todoitems-form').style.display = 'none';
	} catch (error) {
		console.error(error);
	}
};

const toggleCompleted = async (elem, id) => {
	try {
		const description = elem.querySelector('.todoitem-description-span').innerText;

		const icone = elem.querySelector('.todoitem-description-container i');
		const completed = icone.className === 'far fa-check-circle';

		const todoItem = {
			// id,
			description,
			completed,
		};
		
		todoItem.completed = !todoItem.completed;
		const response = await axios.put(`/api/todos/${id}`, todoItem);
		
		if (response.status !== 204) {
			console.table({response});
			return;
		}

		console.log('PUT: the todo with id ' + todoItem.id + ' was updated');

		icone.className = 'far ' + (todoItem.completed ? 'fa-check-circle' : 'fa-circle');

		const span = elem.querySelector('.todoitem-description-span');
		span.style.textDecoration = todoItem.completed ? 'line-through' : 'unset';
	} catch (error) {
		console.error(error);
	}
};

const addTodoitemForm = document.querySelector('.add-new-todoitem-form');

addTodoitemForm.addEventListener('submit', async function (event) {
	event.preventDefault();
	
	const newTodoItemDescription = document.querySelector(
		'#new-todoitem-description-input'
	).value;
	
	const todoItem = {
		description: newTodoItemDescription,
	};
	
	try {
		const todoListId = document.querySelector('.todolist-id').value;
		const response = await axios.post(
			'/api/todos' + (todoListId.length > 0 ? '/' + todoListId : ''),
			todoItem
		);
		
		if (response.status !== 201) {
			console.table({response});
			return;
		}
		
		const newTodoItem = response.data;
		console.log('POST: a new todo was added', newTodoItem);
		appendToDOM(newTodoItem);
	} catch (error) {
		console.error(error);
	}
});

const deleteAllTodoitemsForm = document.querySelector('.delete-all-todoitems-form');

deleteAllTodoitemsForm.addEventListener('submit', async function (event) {
	event.preventDefault();
	
	deleteAllTodoItems();
});

const deleteTodoitemButtons = document.querySelectorAll('.delete-todoitem-button');

deleteTodoitemButtons.forEach((deleteTodoitemButton) => {
	deleteTodoitemButton.addEventListener('click', () => {
		const li = deleteTodoitemButton.parentElement;
		const id = li.id.substring('todoitem-'.length);
		deleteTodoItem(
			li,
			id
		);
	});
});

const liTodoItems = document.querySelectorAll('.task-list li');

liTodoItems.forEach((li) => {
	const id = li.id.substring('todoitem-'.length);
	li.addEventListener('click', () => {
		toggleCompleted(li, id);
	});
});