CREATE TABLE todo_item (
	id UUID NOT NULL PRIMARY KEY,
	id_todo_list UUID NOT NULL REFERENCES todo_list(id_todo_list) ON DELETE CASCADE,
	description VARCHAR(100) NOT NULL,
	done BOOLEAN NOT NULL
);