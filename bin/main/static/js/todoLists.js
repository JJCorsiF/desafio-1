// import axios from 'https://unpkg.com/axios@0.20.0/dist/axios.min.js';

const createLi = (todoList) => {
	const li = document.createElement('li');
	li.className = 'todolist-' + todoList.id;
	
	const todoListDescriptionContainer = document.createElement('div');
	todoListDescriptionContainer.className = 'todolist-name-container';
	
	const todoListNameLink = document.createElement('a');
	todoListNameLink.className = 'todolist-name';
	todoListNameLink.innerHTML = todoList.name;
	todoListNameLink.href = '/web/todos/' + todoList.id;
	
	todoListDescriptionContainer.appendChild(todoListNameLink);
	li.appendChild(todoListDescriptionContainer);
	
	const deleteTodoListButton = document.createElement('button');
	deleteTodoListButton.type = 'button';
	deleteTodoListButton.className = 'delete-todolist-button';
	
	const faDeleteIcon = document.createElement('i');
	faDeleteIcon.className = 'far fa-times-circle'; 
	deleteTodoListButton.appendChild(faDeleteIcon);
	
	deleteTodoListButton.addEventListener('click', () => {
		deleteTodoList(li, todoList.id);
	});
	
	li.appendChild(deleteTodoListButton);
	
	return li;
};

const appendToDOM = (todoList) => {
	const ul = document.querySelector('ul.todolist-list');
	ul.appendChild(createLi(todoList));
	document.querySelector('.no-todolists-to-show').style.display = 'none';
	document.querySelector('.delete-all-todolists-form').style.display = 'flex';
};

const deleteTodoList = async (elem, id) => {
	try {
		const response = await axios.delete(`/api/todolists/${id}`);
		
		if (response.status !== 204) {
			console.table({response});
			return;
		}

		console.log('DELETE: the todolist with id ' + id + ' was removed');
		elem.remove();

		if (document.querySelectorAll('.todolist-list li').length === 0) {
			document.querySelector('.no-todolists-to-show').style.display = 'block';
			document.querySelector('.delete-all-todolists-form').style.display = 'none';
		}
	} catch (error) {
		console.error(error);
	}
};

const deleteAllTodoLists = async () => {
	try {
		const response = await axios.delete(
			'/api/todolists'
		);

		if (response.status !== 204) {
			console.table({ response });
			return;
		}
		
		console.log('DELETE: all todolists were removed');
		document.querySelectorAll('.todolist-list li').forEach((li) => {
			li.remove();
		});

		document.querySelector('.no-todolists-to-show').style.display = 'block';
		document.querySelector('.delete-all-todolists-form').style.display = 'none';
	} catch (error) {
		console.error(error);
	}
};

const addTodoListForm = document.querySelector('.add-new-todolist-form');

addTodoListForm.addEventListener('submit', async function (event) {
	event.preventDefault();
	
	const newTodoListName = document.querySelector(
		'#new-todolist-name-input'
	).value;
	
	const todoList = {
		name: newTodoListName,
	};
	
	try {
		const response = await axios.post('/api/todolists', todoList);
		
		if (response.status !== 201) {
			console.table({response});
			return;
		}
		
		const newTodoList = response.data;
		console.log('POST: a new todolist was added', newTodoList);
		appendToDOM(newTodoList);
	} catch (error) {
		console.error(error);
	}
});

const deleteAllTodoListsForm = document.querySelector('.delete-all-todolists-form');

deleteAllTodoListsForm.addEventListener('submit', async function (event) {
	event.preventDefault();
	
	deleteAllTodoLists();
});

const deleteTodoListButtons = document.querySelectorAll('.delete-todolist-button');

deleteTodoListButtons.forEach((deleteTodoListButton) => {
	deleteTodoListButton.addEventListener('click', () => {
		const li = deleteTodoListButton.parentElement;
		const id = li.id.substring('todolist-'.length);
		deleteTodoList(
			li,
			id
		);
	});
});