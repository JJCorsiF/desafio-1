CREATE DATABASE "todo-app-spring-db";
CREATE USER stefanini WITH ENCRYPTED PASSWORD 'stefanini';
GRANT ALL PRIVILEGES ON DATABASE "todo-app-spring-db" TO stefanini;
ALTER TABLE public.flyway_schema_history OWNER TO stefanini;
ALTER TABLE public.todo_item OWNER TO stefanini;
ALTER TABLE public.todo_list OWNER TO stefanini;